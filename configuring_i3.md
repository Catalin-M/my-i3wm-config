# i3 Configuration  

Configuring i3 isn't as easy as it sounds on Debian when compared to Regolith on Ubuntu, however, it something I wanted to do so here's a quick rundown of the things I did to get it running.  

## ALSA/Pulse - Audio  

With audio I actually tinkered around using [ALSA](https://wiki.debian.org/ALSA) and calling `alsactl init` to initialise it and then `alsactl store` to save settings. But after spending some time with it, I realized that it is not ideal for my use case. What I wanted was just simple access to sound controls. For this purpose I decided to use [PulseAudio](https://wiki.debian.org/PulseAudio) which is installed by default on most Debian desktop envs and it acts as a sound server. A sound server is a background process accepting input from one or more sources and then it can mix/redirect those sources to one or more sinks.

## Screen scaling  

Because of my screen resolution I need to scale the display otherwise everything looks too small. To do this I used `xrandr --dpi 120`. The default is 96 and it is recommended to use multiples of 96, but the scaling I used should equivalate to 125%.  

## Screen Brightness  

The first step would be to find the directory that holds your backlight settings. To find it use `find /sys/ -type f -inname '*brightness*'`. This looks for files that have the word 'brightness' in them. Once this is done you can progress to prep the screen brightness adjustment.

Adjusting the screen brightness is done with `xbacklight`. This also had issues initially due to the way intel video drivers would be recognized. I initially thought that I needed to do a symlink, but the issue was that I was entirely missing the `xorg.conf` file.  

```bash  
Section "Device"
Identifier "Card0"
Driver "intel"
Option "Backlight" "/sys/class/backlight/intel_backlight"
EndSection
```  

The above code was added into */etc/X11/xorg.conf*. If the file is missing, then create it and add those entries.  

## After initial setup  

After the initial setup was complete, the next portion is setting up the actual configuration file. This, for me, is found in *~/.config/i3/config*. The first step is finding out how the keys themselves are named. To find the name of the key, I typed in `xev` inside an open terminal window which then provides a new window. Once the focus is on that window, pressing any keys will show what the name of the event is. For example, the **FN** + **Lower Volume** keys combo for me outputs `XF86AudioRaiseVolume`. Now this can be used to bind the key to a lower volume action.  

To add some more context to the way `pactl` (Pulse Audio) works - here, the sink volume (a sink is a service that accepts signal from an input processed by Pulse Audio - it can be a sound card, remote network PulseAudio servers or other processes) is set to increase or decrease. The `@DEFAULT_SINK@` variable picks up whatever the currently active sink is and applies the volume change to that sink (it could be that I have headphones plugged in or I'm just using the built-in speakers) on the fly without having to handle the different possible scenarios. The `toggle` keyword is simple, it just means that on keypress the sound is on or off.

```bash
# Audio controls
bindsym XF86AudioRaiseVolume exec --no-startup-id pactl set-sink-volume @DEFAULT_SINK@ +3000
bindsym XF86AudioLowerVolume exec --no-startup-id pactl set-sink-volume @DEFAULT_SINK@ -3000
bindsym XF86AudioMute exec --no-startup-id pactl set-sink-mute @DEFAULT_SINK@ toggle

# Brightness controls
bindsym XF86MonBrightnessUp exec xbacklight -inc 10
bindsym XF86MonBrightnessDown exec xbacklight -dec 10
```  

Once I find each key name, I can then setup the binds and link up each key to a specific command. Here I call `amixer`, I select which card to use and increase or decrease the volume. The last option sets the `Master` controller to toggle allowing me to toggle it from on to off on key press. As with most things, you can use `amixer -h` to get help and get you started.  

Below that I have the brightness up and down and the setup is pretty self explanatory.  

## i3 Status Bar  

The next step is configuring the status bar so that it shows the information I am interested in. The status bar configuration file for me was located in */etc/i3status.conf* but I made a copy of it and placed it into my home folder for the user I was using the machine as. As far as the file itself is concerned, there are a few settings in it already, but I started by removing some things and adding some custom settings. Most of this is documented pretty well inside the i3status documentation on the i3wm.org page. As such, all I will do is paste the configuration below.  

```bash

```
